<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.folsomcreative.com
 * @since             1.0.0
 * @package           Eloqua_Mixpanel_Gravityforms
 *
 * @wordpress-plugin
 * Plugin Name:       Eloqua and Mixpanel for Gravity Forms
 * Plugin URI:        https://www.folsomcreative.com
 * Description:       Sends event data to Mixpanel by collecting information from Gravity Forms or Eloqua. If a user doesn't exist in Eloqua, the user is generated upon Gravity Form submission for future use.
 * Version:           1.2.0
 * Author:            Folsom Creative, Inc.
 * Author URI:        https://www.folsomcreative.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       eloqua-mixpanel-gravityforms
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'EMGF_VERSION', '1.2.0' );

require_once plugin_dir_path( __FILE__ ) . 'lib/wp-package-updater/class-wp-package-updater.php';

$folsom_updater = new WP_Package_Updater(
  'https://plugins.folsomcreative.com',
  wp_normalize_path( __FILE__ ),
  wp_normalize_path( plugin_dir_path( __FILE__ ) )
);

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-eloqua-mixpanel-gravityforms-activator.php
 */
function activate_eloqua_mixpanel_gravityforms() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-eloqua-mixpanel-gravityforms-activator.php';
	Eloqua_Mixpanel_Gravityforms_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-eloqua-mixpanel-gravityforms-deactivator.php
 */
function deactivate_eloqua_mixpanel_gravityforms() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-eloqua-mixpanel-gravityforms-deactivator.php';
	Eloqua_Mixpanel_Gravityforms_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_eloqua_mixpanel_gravityforms' );
register_deactivation_hook( __FILE__, 'deactivate_eloqua_mixpanel_gravityforms' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-eloqua-mixpanel-gravityforms.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_eloqua_mixpanel_gravityforms() {

	$plugin = new Eloqua_Mixpanel_Gravityforms();
	$plugin->run();

}
run_eloqua_mixpanel_gravityforms();
