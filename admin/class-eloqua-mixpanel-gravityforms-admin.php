<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://www.folsomcreative.com
 * @since      1.0.0
 *
 * @package    Eloqua_Mixpanel_Gravityforms
 * @subpackage Eloqua_Mixpanel_Gravityforms/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Eloqua_Mixpanel_Gravityforms
 * @subpackage Eloqua_Mixpanel_Gravityforms/admin
 * @author     Folsom Creative, Inc. <hello@folsomcreative.com>
 */
class Eloqua_Mixpanel_Gravityforms_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		add_action( 'admin_notices', [ &$this, 'admin_notices' ] );
		add_action( 'add_meta_boxes', [ &$this, 'add_meta_box' ] );
		add_action( 'save_post', [ &$this, 'save_meta' ] );

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Eloqua_Mixpanel_Gravityforms_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Eloqua_Mixpanel_Gravityforms_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/eloqua-mixpanel-gravityforms-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Eloqua_Mixpanel_Gravityforms_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Eloqua_Mixpanel_Gravityforms_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/eloqua-mixpanel-gravityforms-admin.js', array( 'jquery' ), $this->version, false );

	}

	public static function admin_notices() {
		if ( self::is_gravity_forms_active() ) {
			return;
		} else {
			$class = 'notice notice-warning is-dismissible';
			$message = __( 'Warning: Gravity Forms is not installed or activated. This plugin will not function without Gravity Forms.', 'eloqua-mixpanel-gravityforms' );

			echo sprintf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
		}
	}

	public static function is_gravity_forms_active() {
		return class_exists( 'RGFormsModel' );
	}

	public static function add_meta_box()
    {
		if ( ! self::is_gravity_forms_active() ) {
			return;
		}

		$screens = ['page'];

        foreach ( $screens as $screen ) {
            add_meta_box(
                'emgf_settings',          // Unique ID
                'Eloqua Integration Settings', // Box title
                [&$this, 'meta_box_html'],   // Content callback, must be of type callable
                $screen,                  // Post type
                'advanced'
            );
        }
    }

    public static function save_meta( $post_id )
    {
		$post_type = get_post_type( $post_id );

		if ( "page" !== $post_type || ! self::is_gravity_forms_active() ) {
			return;
		}

        if ( array_key_exists( 'emgf_mixpanel_token', $_POST ) ) {
            update_post_meta(
                $post_id,
                '_emgf_mixpanel_token',
                sanitize_text_field( $_POST['emgf_mixpanel_token'] )
            );
		}

        if ( array_key_exists( 'emgf_cookie_id', $_POST ) ) {
            update_post_meta(
                $post_id,
                '_emgf_cookie_id',
                sanitize_text_field( $_POST['emgf_cookie_id'] )
            );
		}

		if ( array_key_exists( 'emgf_elq_site_id', $_POST ) ) {
            update_post_meta(
                $post_id,
                '_emgf_elq_site_id',
                sanitize_text_field( $_POST['emgf_elq_site_id'] )
            );
		}

		if ( array_key_exists( 'emgf_elq_form_id', $_POST ) ) {
            update_post_meta(
                $post_id,
                '_emgf_elq_form_id',
                sanitize_text_field( $_POST['emgf_elq_form_id'] )
            );
		}

		if ( array_key_exists( 'emgf_visitor_id', $_POST ) ) {
            update_post_meta(
                $post_id,
                '_emgf_visitor_id',
                sanitize_text_field( $_POST['emgf_visitor_id'] )
            );
		}

		if ( array_key_exists( 'emgf_primary_id', $_POST ) ) {
            update_post_meta(
                $post_id,
                '_emgf_primary_id',
                sanitize_text_field( $_POST['emgf_primary_id'] )
            );
        }
	}

	public static function meta_box_html( $post )
    {
      $emgf_mixpanel_token = get_post_meta( $post->ID, '_emgf_mixpanel_token', true );
			$emgf_cookie_id = get_post_meta( $post->ID, '_emgf_cookie_id', true );
			$emgf_elq_site_id = get_post_meta( $post->ID, '_emgf_elq_site_id', true );
			$emgf_elq_form_id = get_post_meta( $post->ID, '_emgf_elq_form_id', true );
			$emgf_visitor_id = get_post_meta( $post->ID, '_emgf_visitor_id', true );
			$emgf_primary_id = get_post_meta( $post->ID, '_emgf_primary_id', true );
        ?>
        <p class="post-attributes-label-wrapper">
			<label class="post-attributes-label" for="emgf_mixpanel_token">
				Mixpanel Token
			</label>
		</p>

        <input type="text" name="emgf_mixpanel_token" id="emgf_mixpanel_token" value="<?php echo $emgf_mixpanel_token ?>" class="widefat">

        <p class="description">
			Enter the token of the Mixpanel Project you want to use.
		</p>

		<p class="post-attributes-label-wrapper">
			<label class="post-attributes-label" for="emgf_cookie_id">
				Popup Maker Cookie ID
			</label>
		</p>

		<input type="text" name="emgf_cookie_id" id="emgf_cookie_id" value="<?php echo $emgf_cookie_id ?>" class="widefat">

		<p class="description">
			Enter the Cookie ID of the PUM you'd like to close if the user has a elqCID.
		</p>

		<p class="post-attributes-label-wrapper">
			<label class="post-attributes-label" for="emgf_elq_site_id">
				Eloqua Site ID
			</label>
		</p>

		<input type="text" name="emgf_elq_site_id" id="emgf_elq_site_id" value="<?php echo $emgf_elq_site_id ?>" class="widefat">

		<p class="description">
			[DESC].
		</p>

		<p class="post-attributes-label-wrapper">
			<label class="post-attributes-label" for="emgf_elq_form_id">
				Eloqua Form ID
			</label>
		</p>

		<input type="text" name="emgf_elq_form_id" id="emgf_elq_form_id" value="<?php echo $emgf_elq_form_id ?>" class="widefat">

		<p class="description">
			[DESC].
		</p>

		<p class="post-attributes-label-wrapper">
			<label class="post-attributes-label" for="emgf_visitor_id">
				Visitor Lookup ID
			</label>
		</p>

		<input type="text" name="emgf_visitor_id" id="emgf_visitor_id" value="<?php echo $emgf_visitor_id ?>" class="widefat">

		<p class="description">
			[DESC].
		</p>

		<p class="post-attributes-label-wrapper">
			<label class="post-attributes-label" for="emgf_primary_id">
				Primary Lookup ID
			</label>
		</p>

		<input type="text" name="emgf_primary_id" id="emgf_primary_id" value="<?php echo $emgf_primary_id ?>" class="widefat">

		<p class="description">
			[DESC].
		</p>
        <?php
    }

}
