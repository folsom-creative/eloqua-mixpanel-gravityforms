<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.folsomcreative.com
 * @since      1.0.0
 *
 * @package    Eloqua_Mixpanel_Gravityforms
 * @subpackage Eloqua_Mixpanel_Gravityforms/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Eloqua_Mixpanel_Gravityforms
 * @subpackage Eloqua_Mixpanel_Gravityforms/includes
 * @author     Folsom Creative, Inc. <hello@folsomcreative.com>
 */
class Eloqua_Mixpanel_Gravityforms_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
