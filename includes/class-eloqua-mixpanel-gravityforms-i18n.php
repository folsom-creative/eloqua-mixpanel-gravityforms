<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://www.folsomcreative.com
 * @since      1.0.0
 *
 * @package    Eloqua_Mixpanel_Gravityforms
 * @subpackage Eloqua_Mixpanel_Gravityforms/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Eloqua_Mixpanel_Gravityforms
 * @subpackage Eloqua_Mixpanel_Gravityforms/includes
 * @author     Folsom Creative, Inc. <hello@folsomcreative.com>
 */
class Eloqua_Mixpanel_Gravityforms_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'eloqua-mixpanel-gravityforms',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
