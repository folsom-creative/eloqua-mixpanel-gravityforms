<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://www.folsomcreative.com
 * @since      1.0.0
 *
 * @package    Eloqua_Mixpanel_Gravityforms
 * @subpackage Eloqua_Mixpanel_Gravityforms/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Eloqua_Mixpanel_Gravityforms
 * @subpackage Eloqua_Mixpanel_Gravityforms/includes
 * @author     Folsom Creative, Inc. <hello@folsomcreative.com>
 */
class Eloqua_Mixpanel_Gravityforms_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
