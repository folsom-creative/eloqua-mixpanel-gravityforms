<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.folsomcreative.com
 * @since      1.0.0
 *
 * @package    Eloqua_Mixpanel_Gravityforms
 * @subpackage Eloqua_Mixpanel_Gravityforms/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Eloqua_Mixpanel_Gravityforms
 * @subpackage Eloqua_Mixpanel_Gravityforms/public
 * @author     Folsom Creative, Inc. <hello@folsomcreative.com>
 */
class Eloqua_Mixpanel_Gravityforms_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		add_action( 'wp_head', [ &$this, 'maybe_add_mixpanel' ], 1 );

		//Gravity Wiz // Gravity Forms // Map Submitted Field Values to Another Field
		//https://gist.github.com/spivurno/7029518
		add_filter( 'gform_pre_validation', array( &$this, 'map_field_to_field' ), 11 );
		add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Eloqua_Mixpanel_Gravityforms_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Eloqua_Mixpanel_Gravityforms_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/eloqua-mixpanel-gravityforms-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Eloqua_Mixpanel_Gravityforms_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Eloqua_Mixpanel_Gravityforms_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		if ( is_page() ) {
			wp_enqueue_script( 'vimeo-player', '//player.vimeo.com/api/player.js', [ 'jquery' ], $this->version, false );
			wp_enqueue_script( 'cookies-js', '//cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js', [ 'jquery' ], $this->version, false );
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/eloqua-mixpanel-gravityforms-public.js', array( 'jquery' ), $this->version, false );

			$page_id = get_the_ID();

			$localized_options = [
				'mixpanel_token' => get_post_meta( $page_id, '_emgf_mixpanel_token', true ),
				'cookie_id' => get_post_meta( $page_id, '_emgf_cookie_id', true ),
				'elq_site_id' => get_post_meta( $page_id, '_emgf_elq_site_id', true ),
				'elq_form_id' => get_post_meta( $page_id, '_emgf_elq_form_id', true ),
				'visitor_id' => get_post_meta( $page_id, '_emgf_visitor_id', true ),
				'primary_id' => get_post_meta( $page_id, '_emgf_primary_id', true ),
			];

			wp_localize_script( $this->plugin_name, 'elq_mxp_options', $localized_options );
		}
	}

	public function maybe_add_mixpanel() {
		if ( ! is_page() ) {
			return;
		}

		$mixpanel_token = get_post_meta( get_the_ID(), '_emgf_mixpanel_token', true );

		if ( ! isset( $mixpanel_token ) ) {
			return;
		}
		?>

		<!-- Mixpanel -->
		<script type="text/javascript">
		(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
		0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.unset people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
		for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);

		//we need to make this value a variable on the page.
		mixpanel.init("<?php echo $mixpanel_token ?>");

		</script>
		<!-- /Mixpanel -->
		<?php
	}

	public function map_field_to_field( $form ) {

    foreach( $form['fields'] as $field ) {

        if( is_array( $field['inputs'] ) ) {
            $inputs = $field['inputs'];
        } else {
            $inputs = array(
                array(
                'id' => $field['id'],
                'name' => $field['inputName']
                )
            );
        }

        foreach( $inputs as $input ) {

            $value = rgar( $input, 'name' );
            if( ! $value )
                continue;

            $post_key = 'input_' . str_replace( '.', '_', $input['id'] );
            $current_value = rgpost( $post_key );

            preg_match_all( '/{[^{]*?:(\d+(\.\d+)?)(:(.*?))?}/mi', $input['name'], $matches, PREG_SET_ORDER );

            // if there is no merge tag in inputName - OR - if there is already a value populated for this field, don't overwrite
            if( empty( $matches ) )
                continue;

            $entry = $this->get_lead( $form );

            foreach( $matches as $match ) {

                list( $tag, $field_id, $input_id, $filters, $filter ) = array_pad( $match, 5, 0 );

                $force = $filter === 'force';
                $tag_field = RGFormsModel::get_field( $form, $field_id );

                // only process replacement if there is no value OR if force filter is provided
                $process_replacement = ! $current_value || $force;

                if( $process_replacement && ! RGFormsModel::is_field_hidden( $form, $tag_field, array() ) ) {

                    $field_value = GFCommon::replace_variables( $tag, $form, $entry );
                    if( is_array( $field_value ) ) {
                      $field_value = implode( ',', array_filter( $field_value ) );
                    }

                } else {

                    $field_value = '';

                }

                $value = trim( str_replace( $match[0], $field_value, $value ) );

            }

            if( $value ) {
                $_POST[$post_key] = $value;
            }

        }

    }

	    return $form;
	}

	public function get_lead( $form ) {

	    if( ! $this->lead )
	        $this->lead = GFFormsModel::create_lead( $form );

	    return $this->lead;
	}


}
