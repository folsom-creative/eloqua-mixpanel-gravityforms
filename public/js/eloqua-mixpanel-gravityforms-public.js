	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	//Web Data Lookup Script
	var LookupIdVisitor = elq_mxp_options.visitor_id;
	var LookupIdPrimary = elq_mxp_options.primary_id;
	var VisitorUniqueField = "V_ElqEmailAddress"; // This doesn't change
	var PrimaryUniqueField = "C_EmailAddress"; // This doesn't change
	var EloquaFormID = elq_mxp_options.elq_form_id;
	var EloquaSiteID = elq_mxp_options.elq_site_id;
	var EloquaScript = "//img.en25.com/i/elqCfg.min.js";

	console.log("LookupIdVisitor= "+LookupIdVisitor);
	console.log("LookupIdPrimary= "+LookupIdPrimary);
	console.log("EloquaFormID= "+EloquaFormID);
	console.log("EloquaSiteID= "+EloquaSiteID);
	console.log("LookupIdVisitor= "+LookupIdVisitor);


	var _elqQ = _elqQ || [];
	_elqQ.push(['elqSetSiteId', EloquaSiteID]);
	_elqQ.push(['elqGetCustomerGUID']); //first try
	_elqQ.push(['elqTrackPageView']);
	(function() {
	  function async_load() {
	    var s = document.createElement('script');
	    s.type = 'text/javascript';
	    s.async = true;
	    s.src = EloquaScript;
	    var x = document.getElementsByTagName('script')[0];
	    x.parentNode.insertBefore(s, x);
	  }
	  if (window.addEventListener) window.addEventListener('DOMContentLoaded', async_load, false);
	  else if (window.attachEvent) window.attachEvent('onload', async_load);
	})();

	// GUID Script
	var timerId = null,
	  timeout = 50;

	function WaitUntilCustomerGUIDIsRetrieved() {
	  if (!!(timerId)) {
			console.log('Trying to set GUID. Tries remaining '+timeout);
	    if (timeout == 0) {
	      return;
	    }
	    if (typeof this.GetElqCustomerGUID === "function") {
	      jQuery('.gform_hidden input[value="elqCustomerGUID"]').val(this.GetElqCustomerGUID());
				console.log('GUID is '+this.GetElqCustomerGUID());
				mixpanel.identify(this.GetElqCustomerGUID());
	      return;
	    }
	    timeout -= 1;
			_elqQ.push(['elqGetCustomerGUID']); // secondary tries
	  }
	  timerId = setTimeout("WaitUntilCustomerGUIDIsRetrieved()", 2500);

	  return;
	}
	window.onload = setTimeout(function () {
		WaitUntilCustomerGUIDIsRetrieved();
	}, 5000);



	// Visitor History Script & Data Script
	var FirstLookup = true;

	function SetElqContent() {
	  if (FirstLookup) {
			console.log("User Lookup - "+GetElqContentPersonalizationValue(VisitorUniqueField));
	    _elqQ.push(['elqDataLookup', escape(LookupIdPrimary), '<' + PrimaryUniqueField + '>' + GetElqContentPersonalizationValue(VisitorUniqueField) + '</' + PrimaryUniqueField + '>']);
	    FirstLookup = false;
	  } else {
	    InputData();
	  }
	}
	_elqQ.push(['elqDataLookup', escape(LookupIdVisitor), '']);

	//Form Population
	function InputData() {
	  (function($) {

	    //set personalization data variables
	    var C_FirstName = this.GetElqContentPersonalizationValue('C_FirstName');
	    var C_LastName = this.GetElqContentPersonalizationValue('C_LastName');
	    var C_Company = this.GetElqContentPersonalizationValue('C_Company');
	    var C_Title = this.GetElqContentPersonalizationValue('C_Title');
	    var C_EmailAddress = this.GetElqContentPersonalizationValue('C_EmailAddress');
	    var C_BusPhone = this.GetElqContentPersonalizationValue('C_BusPhone');
	    var C_Address1 = this.GetElqContentPersonalizationValue('C_Address1');
	    var C_Address2 = this.GetElqContentPersonalizationValue('C_Address2');
	    var C_City = this.GetElqContentPersonalizationValue('C_City');
	    var C_State_Prov = this.GetElqContentPersonalizationValue('C_State_Prov');
	    var C_Zip_Postal = this.GetElqContentPersonalizationValue('C_Zip_Postal');
	    var C_Country = this.GetElqContentPersonalizationValue('C_Country');

	    //write user data to gravityform - form filed must use variable name as css class name
	    $('.C_FirstName input').val(this.GetElqContentPersonalizationValue('C_FirstName'));
	    $('.C_LastName input').val(this.GetElqContentPersonalizationValue('C_LastName'));
	    $('.C_Company input').val(this.GetElqContentPersonalizationValue('C_Company'));
	    $('.C_Title input').val(this.GetElqContentPersonalizationValue('C_Title'));
	    $('.C_EmailAddress input').val(this.GetElqContentPersonalizationValue('C_EmailAddress'));
	    $('.C_BusPhone input').val(this.GetElqContentPersonalizationValue('C_BusPhone'));
	    $('.C_Address1 input').val(this.GetElqContentPersonalizationValue('C_Address1'));
	    $('.C_Address2 input').val(this.GetElqContentPersonalizationValue('C_Address2'));
	    $('.C_City input').val(this.GetElqContentPersonalizationValue('C_City'));
	    $('.C_State_Prov option[value="' + this.GetElqContentPersonalizationValue('C_State_Prov') + '"]').prop('selected', true);
	    $('.C_Zip_Postal input').val(this.GetElqContentPersonalizationValue('C_Zip_Postal'));
	    //$('.C_Country option[value="' + this.GetElqContentPersonalizationValue('C_Country') + '"]').prop('selected', true);

	    //send the users data to mixpanel if they are in eloqua
	    if (C_EmailAddress) {
	      console.log('User Known - MP Data Sent');
				var Code = getParameterByName('elqCID')
	      mixpanel.identify(this.GetElqCustomerGUID());
	      mixpanel.people.set({
	        //set reserved data
	        "$first_name": C_FirstName,
	        "$last_name": C_LastName,
	        "$email": C_EmailAddress,
	        //set custom user data
	        "C_FirstName": C_FirstName,
	        "C_LastName": C_LastName,
	        "C_Company": C_Company,
	        "C_Title": C_Title,
	        "C_EmailAddress": C_EmailAddress,
	        "C_BusPhone": C_BusPhone,
	        "C_Address1": C_Address1,
	        "C_Address2": C_Address2,
	        "C_City": C_City,
	        "C_State_Prov": C_State_Prov,
	        "C_Zip_Postal": C_Zip_Postal,
	        "C_Country": C_Country,
					"Code": Code,
	      });
	      mixpanel.track('Page View');
	      mixpanel.track_links("a", "Link Click", function(ele) {
	        return {
	          'Link Title': jQuery(ele).attr('title'),
	          'Link Path': jQuery(ele).attr('href'),
	          'Link Content': jQuery(ele).text()
	        }
	      });
	    } else {
	      console.log('User Unknown - MP Data Not Sent');
	    }

	  })(jQuery);
	}

	function getParameterByName(name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, "\\$&");
      var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
          results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
  }
	function theOracle() {
    var elqCID = getParameterByName('elqCID');
    if(elqCID){
      Cookies.set(elq_mxp_options.cookie_id, '1', { expires: 30 });
			console.log('Bypass');
    }

  }
	theOracle();

	jQuery(document).bind('gform_confirmation_loaded', function(event, formId) {
	  //force the page to reload when a gravityform is submitted
	  setTimeout(function () {
	  	//window.location.reload();
			WaitUntilCustomerDataIsRetrieved();
		  //_elqQ.push(['elqDataLookup', escape(LookupIdVisitor), '']);
	  }, 1500);
	});

		//Personalization Request
		var dataId = null,
		  dataTimeout = 50;

		function WaitUntilCustomerDataIsRetrieved() {
		  if (!!(dataId)) {
		    if (dataTimeout == 0) {
		      return;
		    }
		    if (typeof this.GetElqContentPersonalizationValue === 'function' && this.GetElqContentPersonalizationValue('V_ElqEmailAddress') != '') {
					console.log('Data Ready');
					_elqQ.push(['elqDataLookup', escape(LookupIdPrimary), '<' + PrimaryUniqueField + '>' + GetElqContentPersonalizationValue(VisitorUniqueField) + '</' + PrimaryUniqueField + '>']);
					return;
				} else {
					_elqQ.push(['elqDataLookup', escape(LookupIdVisitor), '']);
				}

		    dataTimeout -= 1;
		  }

			console.log('Hunting for Data. '+dataTimeout+' tries remaining.');

		  dataId = setTimeout("WaitUntilCustomerDataIsRetrieved()", 1500);

		  return;
		}

	jQuery(document).ready(function() {

	  // track vimeo engagement in mixpanel
	  jQuery("iframe[src*='player.vimeo.com']").each(function() {
	    var videoTitle = '';
	    var iframe = jQuery(this);
	    var player = new Vimeo.Player(iframe);
	    player.getVideoTitle().then(function(title) {
	      videoTitle = title;
	    });
	    player.on('play', function() {
	      mixpanel.track('Video Playback', {
	        'Video Title': videoTitle
	      });
	    });
	  });

	});
